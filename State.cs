﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broadcast
{
    public static class State
    {
        public static UInt16 Port { get; set; } = 36882;

        public static Boolean Debug { get; set; } = false;
    }
}
