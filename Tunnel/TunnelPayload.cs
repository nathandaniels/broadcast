﻿using System;
using System.Net;

namespace Broadcast.Tunnel
{
    [Serializable]
    internal class TunnelPayload
    {
        public UInt16 SourcePort { get; set; }

        public UInt16 DestinationPort { get; set; }

        public IPAddress Destination { get; set; }

        public IPAddress Source { get; set; }

        public Byte[] Payload { get; set; }
    }
}
