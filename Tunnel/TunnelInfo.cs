﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Broadcast.Tunnel
{
    [Serializable]
    public class TunnelInfo
    {
        public ISet<UInt16> ListeningPorts { get; set; }
    }
}
