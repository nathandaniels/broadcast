﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Broadcast.Tunnel
{
    internal class TunnelUdpListener
    {
        private UInt16 Port { get; }

        private ProcessPayload ProcessOutboudPayload { get; }

        private ILogger Logger { get; }

        private UdpClient Client { get; set; }

        private BinaryFormatter Formatter { get; } = new BinaryFormatter();

        private TunnelUdpListener(ushort port, ProcessPayload processOutboudPayload, ILogger logger)
        {
            this.Port = port;
            this.ProcessOutboudPayload = processOutboudPayload;
            this.Logger = logger;
        }
        
        private TunnelUdpListener Start()
        {
            this.Logger.WriteDebugLine($"Starting UDP listener on port {this.Port}");
            this.Client = new UdpClient(this.Port);
            this.Client.BeginReceive(this.ProcessPacket, null);
            return this;
        }

        private void ProcessPacket(IAsyncResult assr)
        {
            IPEndPoint ipe = null;
            var buffer = this.Client.EndReceive(assr, ref ipe);
            this.Client.BeginReceive(this.ProcessPacket, null);

            var payload = new TunnelPayload()
            {
                DestinationPort = this.Port,
                Payload = buffer,
                Source = ipe.Address,
                SourcePort = (UInt16)ipe.Port
            };

            this.ProcessOutboudPayload(payload);
        }

        public static TunnelUdpListener StartNew(ushort port, ProcessPayload processOutboudPayload, ILogger logger)
        {
            return new TunnelUdpListener(port, processOutboudPayload, logger).Start();
        }
    }

    internal delegate void ProcessPayload(TunnelPayload payload);
}
