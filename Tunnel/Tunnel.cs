﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Concurrent;
using System.Threading;

namespace Broadcast.Tunnel
{
    public class Tunnel : IDisposable
    {
        public ISet<UInt16> ListeningPorts { get; private set; }

        private TcpClient TcpClient { get; set; }

        private NetworkStream Stream { get; set; }

        private ILogger Logger { get; }

        private IDictionary<UInt16, TunnelUdpListener> Listeners { get; set; }
        
        private ConcurrentQueue<TunnelPayload> OutboundQueue { get; } = new ConcurrentQueue<TunnelPayload>();

        private Task InboundTask { get; set; }

        private Task OutboundTask { get; set; }

        private CancellationTokenSource TokenSouce { get; } = new CancellationTokenSource();

        public Tunnel(IEnumerable<UInt16> ports, String remoteHost, UInt16 endpointPort, ILogger logger)
        {
            this.ListeningPorts = new HashSet<UInt16>(ports);
            this.Logger = logger;

            this.TcpClient = new TcpClient();

            this.Logger.WriteLine($"Attempting to connect to {remoteHost}:{endpointPort}");

            this.TcpClient.Connect(remoteHost, endpointPort);
            this.Logger.WriteDebugLine("Connected!");
            this.Stream = this.TcpClient.GetStream();
            this.Logger.WriteDebugLine("Sending port information to the other end of the tunnel.");
            new BinaryFormatter().Serialize(this.Stream, new TunnelInfo() { ListeningPorts = this.ListeningPorts });
            this.Logger.WriteLine("Tunnel has been dug.");
        }

        private Tunnel(TunnelInfo info, TcpClient client, NetworkStream stream, ILogger logger)
        {
            this.ListeningPorts = info.ListeningPorts;
            this.Stream = stream;
            this.Logger = logger;
        }

        public static Tunnel WaitForNewTunnel(UInt16 port, ILogger logger)
        {
            logger.WriteLine($"Listening for connections on {IPAddress.Loopback}:{port}");
            var listener = new TcpListener(IPAddress.Loopback, port);

            var client = listener.AcceptTcpClient();
            logger.WriteLine($"Client at {client.Client.RemoteEndPoint} is digging a tunnel...");
            logger.WriteDebugLine("Attempting to read desired tunnel configuration...");

            var stream = client.GetStream();

            try
            {
                var info = new BinaryFormatter().Deserialize(stream) as TunnelInfo;
                logger.WriteLine("Tunnel has been dug.");
                return new Tunnel(info, client, stream, logger);
            }
            catch
            {
                stream.Close();
                throw;
            }
        }

        public void Start()
        {
            this.Listeners = new Dictionary<UInt16, TunnelUdpListener>();
            this.Logger.WriteLine("Starting tunnel control");


            foreach (var port in this.ListeningPorts)
            {
                this.Listeners[port] = (TunnelUdpListener.StartNew(port, this.NewUdpPayload, this.Logger));
            }

            this.Logger.WriteLine($"Started {this.Listeners.Count} UDP listeners");

            var factory = new TaskFactory(this.TokenSouce.Token);

            this.Logger.WriteDebugLine("Starting send and receive threads");

            this.OutboundTask = factory.StartNew(() =>
            {
                var formatter = new BinaryFormatter();
                while (this.OutboundQueue.TryDequeue(out var payload))
                {
                    this.Logger.WriteDebugLine($"Sending ${payload.Payload.Length} bytes from {payload.Source}:{payload.SourcePort} down the tunnel.  Origin port: ${payload.DestinationPort}");
                    formatter.Serialize(this.Stream, payload);
                }
            });

            this.InboundTask = factory.StartNew(() =>
            {
                var formatter = new BinaryFormatter();

                var clients = new UdpClient[UInt16.MaxValue + 1];

                try
                {
                    while (true)
                    {
                        var payload = (TunnelPayload)formatter.Deserialize(this.Stream);

                        if (clients[payload.SourcePort] == null)
                        {
                            this.Logger.WriteDebugLine($"Creating a new sending UDP client for port {payload.SourcePort}");
                            clients[payload.SourcePort] = new UdpClient(payload.SourcePort);
                        }

                        this.Logger.WriteDebugLine($"Received packet from tunnel and forwarding it on.  {payload.Source}:{payload.SourcePort} => localhost:{payload.DestinationPort}");
                        clients[payload.SourcePort].SendAsync(payload.Payload, payload.Payload.Length, "localhost", payload.DestinationPort)
                            .ContinueWith(t => this.Logger.WriteErrorLine(t.Exception.Message), TaskContinuationOptions.OnlyOnFaulted);
                    }
                }
                finally
                {
                    foreach(var client in clients)
                    {
                        if (client != null)
                        {
                            client.Close();
                        }
                    }
                }
            });

            this.Logger.WriteLine("Tunnel is ready.");
        }

        public void Stop()
        {
            this.TokenSouce.Cancel();
            this.TcpClient.Close();
        }

        public void Dispose()
        {
            this.Stop();
        }

        private void NewUdpPayload(TunnelPayload payload)
        {
            this.OutboundQueue.Enqueue(payload);
        }
    }
}
