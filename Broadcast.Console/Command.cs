﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Broadcast.Cli
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public sealed class CommandAttribute : Attribute
    {
        public CommandAttribute(String Command, String Description, String UsageInformation)
        {
            this.Command = Command;
            this.Description = Description;
            this.UsageInformation = UsageInformation;
        }

        public String Command { get; }

        public String Description { get; }

        public String UsageInformation { get; }
    }

    public abstract class Command
    {
        public CommandInfo Info { get; internal set; }

        public IList<String> Arguments { get; internal set; }

        public ILogger Logger { get; internal set; }

        protected abstract void ExecuteCommand();

        public void Execute()
        {
            try
            {
                this.ExecuteCommand();
            }
            catch (Exception e)
            {
                this.Logger.WriteErrorLine(e.Message);
            }
        }

        protected virtual void FailValidation()
        {
            this.Logger.WriteLine(this.Info.UsageInformation);
        }
        
        public class Factory
        {
            public Factory(ILogger logger)
            {
                this.Commands = new Lazy<IDictionary<String, CommandInfo>>(this.LoadCommands);
                this.Logger = logger;
            }

            private Lazy<IDictionary<String, CommandInfo>> Commands { get; }

            private ILogger Logger { get; }

            public Command BuildFromLine(String line)
            {
                var args = Parser.ParseCommandLine(line);

                if (Commands.Value.ContainsKey(args[0]))
                {
                    var info = Commands.Value[args[0]];
                    return info.Construct(args, this.Logger);
                }

                var help = HelpCommand.CreateDefault(this.Logger);
                help.Arguments.Add(args[0]);
                return help;
            }

            internal IList<CommandInfo> GetSortedListOfCommands()
            {
                return Commands.Value.Values.OrderBy(c => c.Command).ToList();
            }

            private IDictionary<String, CommandInfo> LoadCommands()
            {
                var commands = new Dictionary<String, CommandInfo>();

                var thisAssembly = Assembly.GetExecutingAssembly();
                var dir = new FileInfo(thisAssembly.Location).Directory;

                foreach (var path in dir.EnumerateFiles("*.dll").Union(dir.EnumerateFiles("*.exe")))
                {
                    var ass = Assembly.LoadFrom(path.FullName);

                    // If this assembly has any commands
                    if (ass.ExportedTypes.Any(t => Attribute.IsDefined(t,typeof(CommandAttribute))))
                    {
                        foreach (var commmandType in ass.ExportedTypes
                            .Select(t => new
                            {
                                Type = t,
                                Command = Attribute.GetCustomAttribute(t,typeof(CommandAttribute))
                            })
                            .Where(t => t.Command != default(Attribute)))
                        {
                            var info = new CommandInfo(commmandType.Command as CommandAttribute, commmandType.Type);
                            commands[info.Command] = info;

                            if (!typeof(Command).IsAssignableFrom(commmandType.Type))
                            {
                                throw new TypeLoadException($"{commmandType.Type.FullName} has a {nameof(CommandAttribute)} attribute but does not inherit from {nameof(Command)}");
                            }
                        }
                    }
                }

                return commands;
            }
        }

        private static class Parser
        {
            private static Char[] EncapsulationCharacters = { '\'', '"' };

            private static Char[] SplittingCharacters = { ' ' };

            public static IList<String> ParseCommandLine(String line)
            {
                var args = new List<String>(10);
                var builder = new StringBuilder();

                char encapsulator = default(char); 

                foreach (var c in line)
                {
                    if (EncapsulationCharacters.Contains(c))
                    {
                        if (encapsulator == c)
                        {
                            encapsulator = default(char);
                        }
                        else if (encapsulator == default(char))
                        {
                            encapsulator = c;
                        }
                        else
                        {
                            builder.Append(c);
                        }
                    }
                    else if (SplittingCharacters.Contains(c) && encapsulator == default(char))
                    {
                        if (builder.Length > 0)
                        {
                            args.Add(builder.ToString());
                            builder.Clear();
                        }
                    }
                    else
                    {
                        builder.Append(c);
                    }
                }

                if (builder.Length > 0)
                {
                    args.Add(builder.ToString());
                }

                return args;
            }
        }
    }
}
