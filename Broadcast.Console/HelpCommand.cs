﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Broadcast.Cli
{
    [Command("help", "provides help with commands", "Usage: help [<command name>]")]
    public class HelpCommand : Command
    {
        protected override void ExecuteCommand()
        {
            var command = this.Arguments.FirstOrDefault();
            
            if (command == null)
            {
                Console.WriteLine();
                Console.WriteLine("Available Commands:");
                Console.WriteLine();
                Console.WriteLine("{0}{1}", "Command".PadRight(16), "Description");
                Console.WriteLine("{0}{1}", "---------".PadRight(16), "-------------");
                Console.WriteLine(String.Join(Environment.NewLine, new Factory(this.Logger).GetSortedListOfCommands().Select(c => String.Format("{0}{1}", c.Command.PadRight(16), c.Description))));
            }
            else
            {
                var foundCommand = new Factory(this.Logger).BuildFromLine(this.Arguments[0]);

                if (foundCommand.Info.Type != typeof(HelpCommand))
                {
                    this.Logger.WriteLine($"{foundCommand.Info.Command}: {foundCommand.Info.Description}{Environment.NewLine}{foundCommand.Info.UsageInformation}");
                }
                else
                {
                    this.Logger.WriteErrorLine("No such command.  Try calling help for a list of commands.");
                }
            }
        }

        internal static HelpCommand CreateDefault(ILogger logger)
        {
            return new HelpCommand()
            {
                Arguments = new List<String>(),
                Info = new CommandInfo("help", "", "", typeof(Command)),
                Logger = logger
            };
        }
    }
}
