﻿using Broadcast;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Net;

namespace Broadcast.Cli
{
    [Command("udpreceive", "Listens for UDP packets and prints their contents.", "Usage: udreceive [<port>]\r\nPress any key to stop")]
    public class ReceiveCommand : Command
    {
        protected override void ExecuteCommand()
        {
            ushort port = State.Port;
            if (this.Arguments.Count > 0)
            {
                if (!UInt16.TryParse(this.Arguments[0], out port))
                {
                    this.FailValidation();
                    return;
                }
            }

            this.Logger.WriteLine("Listening for messages.  Press any key to cease...");

            using (var client = new UdpClient(port))
            {
                client.BeginReceive(Handle, client);
                Console.ReadKey(true);
            }
        }

        private void Handle(IAsyncResult result)
        {
            try
            {
                var client = result.AsyncState as UdpClient;
                IPEndPoint endpoint = default(IPEndPoint);
                var message = client.EndReceive(result, ref endpoint);

                this.Logger.WriteLine($"Received message from {endpoint}: {Encoding.Unicode.GetString(message)}");
                client.BeginReceive(Handle, client);

            }
            catch (ObjectDisposedException) { }
        }
    }
}
