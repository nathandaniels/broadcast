﻿using Broadcast;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace Broadcast.Cli
{
    [Command("udpsend", "Sends a UDP packet", "Usage: udpsend <destination> <message> [<port>]")]
    public class SendCommand : Command
    {
        protected override void ExecuteCommand()
        {
            ushort port = State.Port;
            if (this.Arguments.Count > 2)
            {
                if (!UInt16.TryParse(this.Arguments[2], out port))
                {
                    this.FailValidation();
                    return;
                }
            }

            if (this.Arguments.Count < 2)
            {
                this.FailValidation();
                return;
            }

            var message = Encoding.Unicode.GetBytes(this.Arguments[1]);

            this.Logger.WriteLine($"Sending packet with {message.Length} bytes to recipient {this.Arguments[0]}:{port}");

            var client = new UdpClient(this.Arguments.First(), port);
            client.Send(message, message.Length);
        }
    }
}
