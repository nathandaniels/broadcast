﻿using System;
using System.Linq;

namespace Broadcast.Cli
{
    [Command("debug", "toggles more verbose output to the console", "usage: debug")]
    public class DebugCommand : Command
    {
        protected override void ExecuteCommand()
        {
            State.Debug = !State.Debug;
            if (!State.Debug)
            {
                this.Logger.WriteLine("debug output is now disabled");
            }
            else
            {
                this.Logger.WriteLine("debug output is now enabled");
            }
        }
    }

    [Command("port", "prints and optionally updates the port to be used for future communication", "usage: port [<new value}]")]
    public class PortCommand : Command
    {
        protected override void ExecuteCommand()
        {
            if (!this.Arguments.Any())
            {
                this.Logger.WriteLine($"current port: {State.Port}");
            }
            else
            {
                if (!UInt16.TryParse(this.Arguments[0], out ushort port))
                {
                    this.FailValidation();
                    return;
                }

                this.Logger.WriteLine($"old port: {State.Port}");
                State.Port = port;
                this.Logger.WriteLine($"new port: {State.Port}");
            }
        }
    }
}
