﻿using System;

namespace Broadcast.Cli
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Broadcast");
            Console.WriteLine("Copyright (C) 2017 Nathan Daniels");

            var factory = new Command.Factory(new ConsoleLogger());
            factory.BuildFromLine("help").Execute();
            Console.WriteLine();
            while (true)
            {
                Console.Write("> ");
                factory.BuildFromLine(Console.ReadLine()).Execute();
                Console.WriteLine();
            }
        }
    }
}
