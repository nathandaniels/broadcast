﻿using System;

namespace Broadcast.Cli
{
    [Command("exit", "Exists the application", "usage: exit")]
    public class ExitCommand : Command
    {
        protected override void ExecuteCommand()
        {
            this.Logger.WriteLine("Farewell");
            Environment.Exit(0);
        }
    }
}
