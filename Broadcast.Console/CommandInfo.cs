﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Broadcast.Cli
{
    public class CommandInfo
    {
        internal CommandInfo(String command, String description, String usageInfo, Type type)
        {
            this.Command = command;
            this.Description = description;
            this.UsageInformation = usageInfo;
            this.Type = type;
        }

        internal CommandInfo(CommandAttribute customAttributeData, Type type)
        {
            this.Type = type;
            this.Command = customAttributeData.Command;
            this.Description = customAttributeData.Description;
            this.UsageInformation = customAttributeData.UsageInformation;
        }

        public String Command { get; }

        public String Description { get; }

        public String UsageInformation { get; }

        internal Type Type { get; set; }

        internal Command Construct(IList<String> args, ILogger logger)
        {
            var command = this.Type.GetConstructor(new Type[0]).Invoke(new object[0]) as Command;
            command.Arguments = args.Skip(1).ToList();
            command.Info = this;
            command.Logger = logger;
            return command;
        }
    }
}
