﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Broadcast.Cli
{
    [Command(
        "tunnelbuilder",
        "digs a TCP tunnel for UDP packets.  There must be someone waiting at the other end",
        "usage: tunnelbuilder <listen port start> <listen port end> <tcp destination> [<tcp port>]")]
    public class TunnelBuilderCommand : Command
    {
        protected override void ExecuteCommand()
        {
            Tunnel.Tunnel t = null;

            try
            {
                var port = State.Port;
                if (this.Arguments.Count > 3)
                {
                    if (!UInt16.TryParse(this.Arguments[3], out port))
                    {
                        this.FailValidation();
                    }
                }

                t = new Tunnel.Tunnel(
                    Enumerable.Range(
                        UInt16.Parse(this.Arguments[0]),
                        UInt16.Parse(this.Arguments[1]))
                        .Select(i=>(ushort)i),
                    this.Arguments[2],
                    port,
                    this.Logger);
            }
            catch (Exception e)
            {
                this.Logger.WriteErrorLine(e.Message);
                this.FailValidation();
            }

            if (t != null)
            {
                t.Start();
                this.Logger.WriteLine("Press 'q' to stop the tunnel");

                while (Console.ReadKey(true).KeyChar != 'q') ;

                t.Stop();
            }
        }
    }
}
