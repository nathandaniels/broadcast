﻿using System;
using System.Threading;

namespace Broadcast.Cli
{
    internal class ConsoleLogger : Logger
    {
        private static SpinLock ColorLock = new SpinLock();

        public override void Write(string message)
        {
            Console.Write(message);
        }

        public override void WriteError(string message)
        {
            Lock();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Error.Write(message);
            Console.ResetColor();
            UnLock();
        }

        public override void WriteErrorLine(string message)
        {
            Lock();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Error.WriteLine(message);
            Console.ResetColor();
            UnLock();
        }

        public override void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        protected override void WriteDebugLineWithoutCheck(string message)
        {
            Lock();
            Console.ForegroundColor = ConsoleColor.DarkGray;
            this.WriteLine(message);
            Console.ResetColor();
            UnLock();
        }

        protected override void WriteDebugWithoutCheck(string message)
        {
            Lock();
            Console.ForegroundColor = ConsoleColor.DarkGray;
            this.Write(message);
            Console.ResetColor();
            UnLock();
        }

        private static void Lock()
        {
            var locked = false;
            while (!locked)
            {
                ColorLock.Enter(ref locked);
            }
        }

        private static void UnLock()
        {
            ColorLock.Exit();
        }
    }
}
