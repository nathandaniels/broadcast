﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broadcast
{
    public abstract class Logger: ILogger
    {
        void ILogger.WriteDebug(String message)
        {
            if (State.Debug)
            {
                this.WriteDebugWithoutCheck(message);
            }
        }

        void ILogger.WriteDebugLine(String message)
        {
            if (State.Debug)
            {
                this.WriteDebugLineWithoutCheck(message);
            }
        }

        protected abstract void WriteDebugWithoutCheck(String message);
        protected abstract void WriteDebugLineWithoutCheck(String message);

        public abstract void Write(string message);
        public abstract void WriteLine(string message);
        public abstract void WriteError(string message);
        public abstract void WriteErrorLine(string message);
    }
}
