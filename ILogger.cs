﻿using System;

namespace Broadcast
{
    public interface ILogger
    {
        void Write(String message);
        void WriteLine(String message);
        void WriteDebug(String message);
        void WriteDebugLine(String message);
        void WriteError(String message);
        void WriteErrorLine(String message);
    }
}
